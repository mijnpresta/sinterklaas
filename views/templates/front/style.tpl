{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<style>

	/* Custom, iPhone Retina */
	@media only screen and (min-width : 320px) {
		.columns-container {
			background-color: #015085!important;
			background-image: url('{$modules_location|escape:'htmlall':'UTF-8'}sinterklaas/views/img/achtergrond.jpg')!important;
			background-repeat: no-repeat!important;
			background-position: center bottom!important;
			background-size: 768px 543px;
			padding-bottom: 220px;
			min-height: 543px;
		}
		.columns-container > .container {
			background: #FFF;
		}
	}

	@media only screen and (min-width : 768px) {
		header {
			background: #015085!important;
		}
		.columns-container {
			background-size: 1920px 1357px;
			padding-bottom: 700px;
			min-height: 1696px;
		}
		.columns-container > .container {
			background: #FFF;
			padding-left: 10px;
			padding-right: 10px;
		}
		#home-page-tabs {
			margin-left: 0;
		}
	}


	@media only screen and (min-width : 768px) {
		.columns-container {
			background-size: 992px 701px;
			padding-bottom: 300px;
			min-height: 701px;
		}
		.columns-container > .container {
			background: #FFF;
			padding-left: 10px;
			padding-right: 10px;
		}
		#home-page-tabs {
			margin-left: 0;
		}
	}

	@media only screen and (min-width : 992px) {
		.columns-container {
			background-size: 1920px 1357px;
			padding-bottom: 500px;
			min-height: 1357px;
		}
	}

	@media only screen and (min-width : 1920px) {
		header {
			background: #015085!important;
		}
		.columns-container {
			background-size: 2400px 1696px;
			padding-bottom: 700px;
			min-height: 1696px;
		}
	}
</style>